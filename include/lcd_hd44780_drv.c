/******************************************************************************
*                     BTG   Electronics Design   B.V.
*******************************************************************************
*
* Client . . . . . .: BSP
* Project  . . . . .: Handheld reader
* Filename . . . . .: lcd_hd44780_drc.c   
*
*******************************************************************************
*
* This module handles the lcd display
*
*
*******************************************************************************
*  Date       By     Description
*  ---------- ----- -----------------------------------------------------------
*  17-10-2013 JWa   First implementation
*  19-11-2014 JWa   Edit for NIOSII spectrum analyzer
******************************************************************************/
#include "globaldefs.h"
#include "dl_delay.h"

#include "lcd_hd44780_drv.h"

#include "altera_avalon_pio_regs.h"

#define 	LCD_DIR				  GLB_GET_REGISTER(GLB_LCD_IO_BASE, 4)
#define 	LCD_OUT				  GLB_GET_REGISTER(GLB_LCD_IO_BASE, 0)

#define		LCD_PIN_BL			  (1 << 14)
#define		LCD_PIN_ON			  (1 << 1)
#define     LCD_PIN_RW            (1 << 4)
#define     LCD_PIN_RS            (1 << 3)
#define     LCD_PIN_EN            (1 << 5)
#define     LCD_PIN_D7            (1 << 13)
#define     LCD_PIN_D6            (1 << 12)
#define     LCD_PIN_D5            (1 << 11)
#define     LCD_PIN_D4            (1 << 10)
#define     LCD_PIN_D3            (1 << 9)
#define     LCD_PIN_D2            (1 << 8)
#define     LCD_PIN_D1            (1 << 7)
#define     LCD_PIN_D0            (1 << 6)
#define     LCD_PIN_START         (6)

#define     LCD_PIN_MASK  ((LCD_PIN_RW | LCD_PIN_RS | LCD_PIN_EN | LCD_PIN_D7 | \
                            LCD_PIN_D6 | LCD_PIN_D5 | LCD_PIN_D4 | LCD_PIN_D3 | \
                                LCD_PIN_D2 | LCD_PIN_D1 | LCD_PIN_D0))

#ifndef     TRUE
#define     FALSE                 0
#define     TRUE                  1
#endif

#define     BACKGROUND_REF_RATE   100//hz

/******************************************************************************
* Local functions.
******************************************************************************/
static void lcd_PulseLCD()
{

    LCD_OUT &= ~LCD_PIN_EN;
    dl_delayms(1);

    LCD_OUT |= LCD_PIN_EN;
    dl_delayms(1);

    LCD_OUT &= (~LCD_PIN_EN);
    dl_delayms(1);
}

static void lcd_SendByte(unsigned char cByteToSend, int iIsData)
{
    LCD_OUT &= (~LCD_PIN_MASK);
    LCD_OUT |= (cByteToSend << LCD_PIN_START);

    if (iIsData == TRUE)
    {
        LCD_OUT |= LCD_PIN_RS;
    }
    else
    {
        LCD_OUT &= ~LCD_PIN_RS;
    }
    
    lcd_PulseLCD();
}


/******************************************************************************
* Global functions.
******************************************************************************/
//Set the cursor position at row and colomn.
void lcd_LCDSetCursorPosition(char cRow, char cCol)
{
    char cAddress;
    if (cRow == 0)
    {
        cAddress = 0;
    }
    else
    {
        cAddress = 0x40;
    }

    cAddress |= cCol;

    lcd_SendByte(0x80 | cAddress, FALSE);
}


void lcd_ClearLCDScreen()
{
    lcd_SendByte(0x01, FALSE);
    lcd_SendByte(0x02, FALSE);
}


//Initialize the lcd screen.
void lcd_InitializeLCD(void)
{
    LCD_DIR = LCD_PIN_MASK | LCD_PIN_ON;
    LCD_OUT = ~(LCD_PIN_MASK | LCD_PIN_ON);

    LCD_OUT |= LCD_PIN_ON;

    dl_delayms(25);
    lcd_SendByte(0x38, FALSE);  //8Bit mode
    dl_delayms(5);
  
    lcd_SendByte(0x0C, FALSE);  //Enable display / Non blinking cursor
    lcd_SendByte(0x06, FALSE);  
}

//Print a string at the first and second line of the lcd.
void lcd_PrintStrFull(unsigned char *ucpText)
{
    lcd_LCDSetCursorPosition(0, 0);
    
    unsigned char *c;

    c = ucpText;

    while ((c != 0) && (*c != 0))
    {
        if(16 == c - ucpText){
            lcd_LCDSetCursorPosition(1, 0);
        }
        lcd_SendByte(*c, TRUE);
        c++;
    }
}

//Print a string at the row pointed by 'row'
void lcd_PrintStr(char cRow, unsigned char *ucpText)
{
    //lcd_SendByte(0x02, FALSE);
    lcd_LCDSetCursorPosition(cRow, 0);
    
    unsigned char *c;

    c = ucpText;

    while ((c != 0) && (*c != 0))
    {
        lcd_SendByte(*c, TRUE);
        c++;
    }
}


