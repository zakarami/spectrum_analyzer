#pragma once
#define DARK_RED 0x16000
#define TURQUOISE 0x12412
#define BLACK 0x0
#define BLUE 0x3200
#define LIGHT_BLUE 0x3600

#include "altera_up_avalon_video_pixel_buffer_dma.h"

alt_up_pixel_buffer_dma_dev* pixel_buf_dev;

void vga_init(int x, int y);
void vga_clearScreen();
void vga_writePixel(int x, int y, short color);
void vga_drawStraightLine(int x1, int y1, int x2, int y2, short color);
void vga_drawBox(int x1, int y1, int x2, int y2, short pixel_color);
void vga_swapBuffer();
void vga_writeChar(int x, int y, char c, int color);
void vga_writeString(int x, int y, char* c, int color);
int vga_bufferStatus();
void vga_waitForBufferSwap();
void vga_drawGraph(int param[], int size, int color);
void vga_writePixelToAddr(volatile short *vga_addr, int x, int y, short colour);
void vga_clearScreenWithAddr();
void vga_drawBlocks(int param[], int size, int color);
