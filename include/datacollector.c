/*
 * datacollector.c
 *
 *  Created on: 14 nov. 2014
 *      Author: jesse
 */
#include "globaldefs.h"
#include "datacollector.h"

#include "altera_up_avalon_audio.h"
#include "altera_up_avalon_audio_and_video_config.h"

#define SAMPLE_CHANN		ALT_UP_AUDIO_RIGHT
#define READ_BUFF_SIZE		1024

alt_up_audio_dev *audiodev = 0;
alt_up_av_config_dev *avcondev = 0;


int dc_initDataCollector(void)
{
	if(!audiodev){
		audiodev = alt_up_audio_open_dev(GLB_AUDIO_DEV);
		avcondev = alt_up_av_config_open_dev(GLB_AV_CON_DEV);
	}
	if(!audiodev || !avcondev){
		return 1;
	}
	alt_up_audio_reset_audio_core(audiodev);
	//alt_up_audio_enable_read_interrupt(audiodev);
	return 0;
}

unsigned int dc_getData(unsigned int *buff, unsigned int len)
{
	unsigned int readAmount = 0;
	readAmount = alt_up_audio_read_fifo_avail(audiodev, SAMPLE_CHANN);
	if(readAmount){
		return alt_up_audio_record_r(audiodev, buff, len);
	}
	return 0;
}
