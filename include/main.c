#include "globaldefs.h"
#include "dl_delay.h"
#include "datacollector.h"
#include "lcd_hd44780_drv.h"
#include "altera_avalon_pio_regs.h"
#include "display.h"

#include "sys/alt_sys_init.h"

#define TST_BUFF_SIZE	64

typedef enum {
	E_AMLITUDE = 0,
	E_FMIN = 1,
	E_FMAX = 2,
	E_NONE = 3
}EMenuState;

unsigned int testbuffer[TST_BUFF_SIZE] = {0};

int hoeveelheiddata=2390;
int Data[2390];
int Buffer[2390];
int GroteBuffer=0;
EMenuState state = E_AMLITUDE;
int stateblock=0;
int KEYblock2=0;
int KEYblock1=0;
int Fmax=2389;
int Fmin=1;
int KEY3=1;
int KEY2=0;
int KEY1=0;

void menuselect(int K2,int K1,int statef);
void freqfilter(int min, int max);
void Ampfilter(int versterking);
void Testen(void);
void trapError(void);

int main () {
   alt_sys_init();

   dl_initDelay();

   if(dc_initDataCollector()){
	   trapError();
   }

   lcd_InitializeLCD();
   lcd_ClearLCDScreen();

   vga_clearScreen();

   lcd_PrintStr(0, (unsigned char*)"EEP71 Spectrum  ");
   lcd_PrintStr(1, (unsigned char*)"Analyzer        ");

   while(!GLB_GET_KEY_STATUS(GLB_KEY0)){}

   Testen();

   while(1){
	   vga_writeString(50, 50, "1.234", 1111);
	   KEY1 = GLB_GET_KEY_STATUS(GLB_KEY1);
	   KEY2 = GLB_GET_KEY_STATUS(GLB_KEY2);
	   KEY3 = GLB_GET_KEY_STATUS(GLB_KEY3);

       if (KEY3==1 && stateblock==0){
           stateblock=1;
           state++;
           if (state >= E_NONE){
               state = E_AMLITUDE;
           }
       }
       else if (KEY3==0 && stateblock==1){
           stateblock=0;
       }
       menuselect(KEY2,KEY1,state);
       //printf("\nFmax:%i\tFmin:%i\n",Fmax,Fmin);
   }
   return 0;
}

void trapError(void)
{
	while(1){

	}
}

///////////////////////////////////////////////////////////////////////
void menuselect(int K2,int K1,int statef){
switch (statef){
    case E_AMLITUDE: //amplitude
    	vga_drawBox(100, 100, 200, 200 , 15600);
    	lcd_PrintStr(0, (unsigned char*)"Amplitude sett  ");
    	lcd_PrintStr(1, (unsigned char*)"                ");
        //printf("\nstate:1\n");
        freqfilter(Fmin,Fmax);
        if (K1==1 && KEYblock1==0){
            KEYblock1=1;
            Ampfilter(0);
        }
        else if (K1==0 && KEYblock1==1){
            KEYblock1=0;
        }
        if (K2==1 && KEYblock2==0){
            KEYblock2=1;
            Ampfilter(1);
        }
        else if (K2==0 && KEYblock2==1){
            KEYblock2=0;
        }
    break;
    case E_FMAX: //maximale frequentie
    	vga_drawBox(100, 100, 200, 200 , 10600);
    	lcd_PrintStr(0, (unsigned char*)"FMAX settings   ");
    	lcd_PrintStr(1, (unsigned char*)"                ");
        //printf("\nstate:2\n");
        if (K1==1 && KEYblock1==0 && Fmax>1200){
            KEYblock1=1;
            Fmax--;
            if (Fmax==1200){
                Fmax++;
            }
        }
        else if (K1==0 && KEYblock1==1){
            KEYblock1=0;
        }
        if (K2==1 && KEYblock2==0 && Fmax<2390){
            KEYblock2=1;
            Fmax++;
            if (Fmax==2390){
                Fmax--;
            }
        }
        else if (K2==0 && KEYblock2==1){
            KEYblock2=0;
        }
        freqfilter(Fmin,Fmax);
    break;
    case E_FMIN: //minimale frequentie
    	vga_drawBox(100, 100, 200, 200 , 5600);
    	lcd_PrintStr(0, (unsigned char*)"FMIN settings   ");
    	lcd_PrintStr(1, (unsigned char*)"                ");
        //printf("\nstate:3\n");
        if (K1==1 && KEYblock1==0 && Fmin>0){
            KEYblock1=1;
            Fmin--;
            if (Fmin==0){
                Fmin++;
            }
        }
        else if (K1==0 && KEYblock1==1){
            KEYblock1=0;
        }
        if (K2==1 && KEYblock2==0 && Fmin<1190){
            KEYblock2=1;
            Fmin++;
            if (Fmin==1190){
                Fmin--;
            }
        }
        else if (K2==0 && KEYblock2==1){
            KEYblock2=0;
        }
        freqfilter(Fmin,Fmax);
    break;
    }
}
/////////////////////////////////////////////////////////////////////////
void freqfilter(int min, int max){
    int i=0;
    GroteBuffer=0;
    for (i=0;i<=hoeveelheiddata;i++){
        if (i<=max&&i>=min){
            Buffer[GroteBuffer]=Data[i];
            GroteBuffer++;
        }
    }
}
/////////////////////////////////////////////////////////////////////////////
void Ampfilter(int versterking){
    int i=0;
    if (versterking==1){
        for (i=0;i<GroteBuffer;i++){
            Buffer[i]=Buffer[i]<<1;
        }
    }
    else {
        for (i=0;i<GroteBuffer;i++){
            Buffer[i]=Buffer[i]>>1;
        }
    }
}
//////////////////////////////////////////////////////////////////////////////////shit
void Testen(){
    int i=0;
    for (i=0;i<=hoeveelheiddata;i++){
        Data[i]=i;
    }
}
