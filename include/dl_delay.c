/*
 * dl_delay.c
 *
 *  Created on: 20 nov. 2014
 *      Author: jesse
 */
#include "globaldefs.h"
#include "dl_delay.h"

#include "sys/alt_timestamp.h"
#include "altera_avalon_timer_regs.h"

#define TIMER_BASE		0x00081000

uint32_t timerCount = 0;

int dl_initDelay(void)
{
	alt_timestamp_start();
	IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_BASE, ((1 << 1)|(1 << 2)));//Set timer to cont and start
	return 0;
}
void dl_delayms(int delay)
{
	volatile alt_timestamp_type timerStart = 0, delayms = 0;
	delayms = delay * 50000;
	//while(1){
		timerStart = alt_timestamp();
	//}
	while((alt_timestamp() - timerStart) < delayms){
	}
}
