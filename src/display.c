//Display.c
//Meant as a drawing library
// --Zack

#include "display.h"
#include <string.h>
#include "sys/alt_stdio.h"

#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })

int screenX;
int screenY;

//call this before using any of the other methods
void vga_init(int x, int y){
	screenX = x;
	screenY = y;
	pixel_buf_dev = alt_up_pixel_buffer_dma_open_dev("/dev/Pixel_Buffer_DMA");
	// open the Pixel Buffer  = alt_up_pixel_buffer_open_dev ("/dev/Pixel_Buffer");
	if (pixel_buf_dev == NULL)
		alt_printf("Error: could not open pixel buffer device \n");
	vga_clearScreenWithAddr();
}

//Clears screen using writepixel to write the whole screen in black
void vga_clearScreen() {
	alt_up_pixel_buffer_dma_clear_screen(pixel_buf_dev, 1);
}

//Draws a single pixel
//x in [0,319], y in [0,239], and colour in [0,65535]
void vga_writePixel(int x, int y, short colour) {
	x = CLAMP(x, 0, screenX);
	y = CLAMP(y, 0, screenY);
	alt_up_pixel_buffer_dma_draw(pixel_buf_dev, colour, x, y);
}

//Draws straight line between two points, using the Bresenham line algorithm
void vga_drawStraightLine(int x1, int y1, int x2, int y2, short colour) {
	x1 = CLAMP(x1, 0, screenX);
	x2 = CLAMP(x2, 0, screenX);
	y1 = CLAMP(y1, 0, screenY);
	y2 = CLAMP(y2, 0, screenY);
	alt_up_pixel_buffer_dma_draw_line(pixel_buf_dev, x1, y1, x2, y2, colour, 1);
}

//draws a box, filled
void vga_drawBox(int x1, int y1, int x2, int y2, short pixel_color) {
	x1 = CLAMP(x1, 0, screenX);
	x2 = CLAMP(x2, 0, screenX);
	y1 = CLAMP(y1, 0, screenY);
	y2 = CLAMP(y2, 0, screenY);
	alt_up_pixel_buffer_dma_draw_box(pixel_buf_dev, x1, y1, x2, y2, pixel_color, 1);
}

//swaps back and front buffer
void vga_swapBuffer() {
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buf_dev);
}

//if returns 0, buffer is done swapping
//if returns 1, buffer is still busy
int vga_bufferStatus(){
	return alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buf_dev);
}

void vga_waitForBufferSwap(){
	while (vga_bufferStatus() == 1);
}

//Clears screen using writepixel to write the whole screen in black
void vga_clearScreenWithAddr() {
	int x, y;
	int color = 0;
	for (x = 0; x < screenX*2; x++) {
		for (y = 0; y < screenY*2; y++) {
			vga_writePixelToAddr(0, x, y, color);
		}
	}
}

//Draws a single pixel
//to a given address (such as 0x0)
void vga_writePixelToAddr(volatile short *vga_addr, int x, int y, short colour) {
	int offset = (y << 9) + x;;
	*(vga_addr + offset) = colour;
}

void vga_drawGraph(int param[], int size, int color){
	int x = 0;
	size = size/sizeof(int);
	int y = 0;
	int oldY = 0;
	int oldX = 0;
	int i;
	 for (i = 0; i < size; i++)
	  {
		 y = param[i];
		 vga_drawStraightLine(oldX*20, 1-(oldY*20)+screenY, x*20, 1-(y*20)+screenY, color);
		 vga_drawBox(x*20-1, 1-(y*20)+screenY-1, (x*20)+1, 1-((y*20)+2)+screenY, 0x3600);
		 oldX = x;
		 oldY = y;
		 x++;
	  }
}

void vga_drawBlocks(int param[], int size, int color){
	int x = 0;
	size = size/sizeof(int);
	int y = 0;
	int i;
	 for (i = 0; i < size; i++)
	  {
		 y = param[i];
		 int j;
		 if(y == 0)
			 y = 1;
		 for(j = y; j > 0; j--)
		 {
			 vga_drawBox(5 + x*20-1, 1-(j*20)+screenY-1+15, 5 + (x*20)+15, 1-((j*20)+4)+screenY+15, color);
		 }
		 x++;
	  }
}

//writes a string using the writechar method
void vga_writeString(int x, int y, char* c, int color){
	int count = strlen(c);
	int i;
	int charSpacing = 5;
	for(i = 0; i < count; i++)
	{
		vga_writeChar(x + (charSpacing * i), y, c[i], color);
	}
}

//writes a single char, supported chars:
// 0, 1, 2, 3, 4, 5, 6, 7, 8, ".", " "
void vga_writeChar(int x, int y, char c, int color) {
	switch (c) {
	case '1':
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 3, y + 1, color);
		vga_writePixel(x + 3, y + 2, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 3, y + 4, color);
		vga_writePixel(x + 1, y + 5, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 4, y + 5, color);
		break;
	case '2':
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 4, y + 2, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 2, y + 4, color);
		vga_writePixel(x + 1, y + 5, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 4, y + 5, color);
		break;
	case '3':
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 4, y + 2, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 1, y + 0, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 1, y + 5, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		break;
	case '4':
		vga_writePixel(x + 1, y + 0, color);
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 1, y + 2, color);
		vga_writePixel(x + 1, y + 3, color);
		vga_writePixel(x + 4, y + 0, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 4, y + 2, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 2, y + 3, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 4, y + 5, color);
		break;
	case '5':
		vga_writePixel(x + 1, y + 0, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 4, y + 0, color);
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 1, y + 2, color);
		vga_writePixel(x + 1, y + 0, color);
		vga_writePixel(x + 2, y + 2, color);
		vga_writePixel(x + 3, y + 2, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 1, y + 5, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		break;
	case '6':
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 1, y + 2, color);
		vga_writePixel(x + 1, y + 3, color);
		vga_writePixel(x + 1, y + 4, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 2, y + 2, color);
		vga_writePixel(x + 3, y + 2, color);
		break;
	case '7':
		vga_writePixel(x + 1, y + 0, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 4, y + 0, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 3, y + 2, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 2, y + 4, color);
		vga_writePixel(x + 2, y + 5, color);
		break;
	case '8':
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 2, y + 2, color);
		vga_writePixel(x + 3, y + 2, color);
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 1, y + 3, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 1, y + 4, color);
		vga_writePixel(x + 4, y + 4, color);
		break;
	case '9':
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 4, y + 2, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 4, y + 5, color);
		break;
	case '0':
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 2, y + 0, color);
		vga_writePixel(x + 3, y + 0, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 4, y + 2, color);
		vga_writePixel(x + 4, y + 3, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 1, y + 2, color);
		vga_writePixel(x + 1, y + 3, color);
		vga_writePixel(x + 1, y + 4, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 2, y + 5, color);
		break;
	case '.':
		vga_writePixel(x + 2, y + 5, color);
		vga_writePixel(x + 2, y + 4, color);
		vga_writePixel(x + 3, y + 5, color);
		vga_writePixel(x + 3, y + 4, color);
		break;
	case ' ':
		break;
	default:
		vga_writePixel(x + 0, y + 0, color);
		vga_writePixel(x + 1, y + 1, color);
		vga_writePixel(x + 2, y + 2, color);
		vga_writePixel(x + 3, y + 3, color);
		vga_writePixel(x + 4, y + 4, color);
		vga_writePixel(x + 5, y + 5, color);
		vga_writePixel(x + 0, y + 5, color);
		vga_writePixel(x + 1, y + 4, color);
		vga_writePixel(x + 2, y + 3, color);
		vga_writePixel(x + 3, y + 2, color);
		vga_writePixel(x + 4, y + 1, color);
		vga_writePixel(x + 5, y + 0, color);
		break;
	}


}
