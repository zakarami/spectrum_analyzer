/*
 * main.c
 *
 *  Created on: 19 nov. 2014
 *      Author: Zack
 */
#include <stdio.h>
#include <stdlib.h>
#include "display.h"
#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "stdlib.h"

int main(void) {
	vga_init(320, 240);

	// clear the buffers
	vga_clearScreen();
	//	while (1) {
	//
	//		// clear the back buffer
	//		alt_up_pixel_buffer_dma_clear_screen(pixel_buf_dev, 1);
	//		// draw something to the back buffer
	//		alt_up_pixel_buffer_dma_draw_box(pixel_buf_dev, 0, 0, 50, 50,
	//				LIGHT_BLUE, 1);
	//		// call swap buffers
	//		alt_up_pixel_buffer_dma_swap_buffers(pixel_buf_dev);
	//
	//		// wait for the operation to complete
	//
	//
	//	}


	/* these variables are used for a box */
	int blue_x1;
	int blue_y1;
	int blue_x2;
	int blue_y2;
	int screen_x;
	int screen_y;
	short color;

	/* create messages to be displayed on the VGA display */

	/* the following variables give the size of the pixel buffer */
	screen_x = 319;
	screen_y = 239;
	color = 0x0; //black
	blue_x1 = 28;
	blue_x2 = 52;
	blue_y1 = 26;
	blue_y2 = 34;
	//0x12412;	// a green blue color
	//0x16000;	// dark red color

	int testGraph[17];
	//{0, 2, 0, 4, 6};
	testGraph[0] = 0;
	testGraph[1] = 2;
	testGraph[2] = 0;
	testGraph[3] = 4;
	testGraph[4] = 3;
	testGraph[5] = 6;
	testGraph[6] = 3;
	testGraph[7] = 8;
	testGraph[8] = 0;
	testGraph[9] = 2;
	testGraph[10] = 3;
	testGraph[11] = 8;
	testGraph[12] = 0;
	testGraph[13] = 2;
	testGraph[14] = 3;
	testGraph[15] = 8;
	testGraph[16] = 0;

	int graphTimer = 0;

	while (1) {
		vga_clearScreen();
		//vga_drawBox(blue_x1, blue_y1, blue_x2, blue_y2, color);
		color += 400;
		graphTimer++;
		if(graphTimer > 5)
		{
			graphTimer = 0;
			int i;
			for(i = 0; i < 16; i++)
			{
				testGraph[i] = rand()%12;
			}
		}

		vga_drawBlocks(testGraph, sizeof(testGraph), color);
		vga_drawGraph(testGraph, sizeof(testGraph), color/2);
		//vga_drawBox(319 - blue_x1, 239 - blue_y1, blue_x2, blue_y2, color);
		vga_swapBuffer();
		//wait for buffer to finish swapping
		vga_waitForBufferSwap();
	}
}
