/******************************************************************************
*                     BTG   Electronics Design   B.V.
*******************************************************************************
*
* Client . . . . . .: BSP
* Project  . . . . .: Handheld reader
* Filename . . . . .: lcd_hd44780_drc.h   
*
*******************************************************************************
*
* This module handles the lcd display
*
*
*******************************************************************************
*  Date       By     Description
*  ---------- ----- -----------------------------------------------------------
*  17-10-2013 JWa   First implementation
******************************************************************************/
#ifndef _LCD_HD_44780_DRV_H
#define _LCD_HD_44780_DRV_H

#define LCD_MAX_COLS        16
#define LCD_MAX_ROWS        2

void lcd_LCDSetCursorPosition(char cRow, char cCol);
void lcd_ClearLCDScreen(void);
void lcd_SetBackgroundLight(int iPercentage);
void lcd_InitializeLCD(void);
void lcd_PrintStrFull(unsigned char *cpText);
void lcd_PrintStr(char cRow, unsigned char *cpText);

#endif
