/*
 * datacollector.h
 *
 *  Created on: 14 nov. 2014
 *      Author: jesse
 */

int dc_initDataCollector(void);
unsigned int dc_getData(unsigned int *buff, unsigned int len);
