#ifndef DL_DELAY_H
#define DL_DELAY_H

int dl_initDelay(void);
void dl_delayms(int delay);

#endif
