/*
 * globaldefs.h
 *
 *  Created on: 14 nov. 2014
 *      Author: jesse
 */
#ifndef GLOBALDEFS_H
#define GLOBALDEFS_H

#include <inttypes.h>
#include <stdbool.h>

//All the devices used:
#define GLB_AUDIO_DEV	"/dev/audio_in"
#define GLB_VIDEO_DEV   "/dev/Pixel_Buffer"
#define GLB_AV_CON_DEV  "/dev/audio_and_video_config"

#define GLB_LCD_IO_BASE	0x00081050
#define GLB_BUT_IO_BASE	0x00081040

#define GLB_GET_REGISTER(base, offset) *((volatile uint32_t*) ((base) + (offset)) )

#define GLB_KEY0	(1 << 0)
#define GLB_KEY1	(1 << 1)
#define GLB_KEY2	(1 << 2)
#define GLB_KEY3	(1 << 3)

#define GLB_GET_KEY_STATUS(key)	(!(GLB_GET_REGISTER(GLB_BUT_IO_BASE, 0) & (key)))

#endif
